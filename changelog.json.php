<?php
/*
{
    "0.6": [
        "在ActionHandler.js增加show_dialog(title, body, footer)，方便產生對話框",
        "將shieldui改成由自己的server載入，不再從原始shieldui官方網站載入，以改善不穩定問題與載入速度過慢的問題",
        "changelog.json改成changelog.json.php，以避免可能的漏洞，也使所有格式統一",
        "在include中增加config_json類別，使所有json格式的資料存取一致化",
        "修改在ActionHandler中的run()為run(args)，更方便資料或參數的傳遞"
    ],
    "0.5.1": [
        "原先的session儲存email，改成儲存user id",
        "Bug：原來的config.json更改成config.json.php，以避免安全性問題"
    ],
    "0.5": [
        "AJAX改用JQuery格式呼叫",
        "原先的showResult()函數改成三個函數：showResult()只適用於沒有呼叫PHP的結果處理，ajax_success()適用於AJAX呼叫成功後的結果處理，ajax_error()適用於AJAX呼叫成功後的結果處理",
        "範例內容中的HTML元素選取方式，全部採用JQuery的語法",
        "Bug：修訂新增使用者時，密碼設定沒有經過md5編碼",
        "Bug：修正role_management模組中，有許多名稱寫錯，仍然呼叫role_profile"
    ],
    "0.4": [
        "增加動態載入CSS功能：loadModuleCSS(module, fileName), loadCSS(src, fileName)",
        "增加動態移除CSS功能：removeModuleCSS(module, fileName), removeCSS(src, fileName)",
        "更改changelog.json內容的排列方式為由新到舊排列"
    ],
    "0.3": [
        "創建changelog，記錄各版本的演進",
        "刪除重覆載入jquery-1.11.1.mis.js",
        "密碼改以md5方式編碼",
        "Bug:修正登入成功後，無法自動跳到顯示頁"
    ],
    "0.2": [
        "支援Jquery, Bootstrap, ShieldUI"
    ],
    "0.1": [
        "加入test_mode功能，可關閉權限控管，方便功能開發"
    ]
}
*/
?>