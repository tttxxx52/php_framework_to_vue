<?php
    class config_json{
        static function read($filename){
            $string = file_get_contents($filename);
            $order = array("<?php", "?>", '/*', "*/");
            $string = str_replace($order, "", $string);
            $config = json_decode($string, true);
            return $config;
        }
    }
?>