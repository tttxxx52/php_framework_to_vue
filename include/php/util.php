<?php
    class util
    {
        public static function __callStatic($name, $args)
        {
            switch ($name) {
                case 'returnValue':
                    if (2 === count($args)) {
                        $obj['status_code'] = $args[0];
                        $obj['message'] = $args[1];

                        return json_encode($obj);
                    } elseif (3 === count($args)) {
                        $obj['status_code'] = $args[0];
                        $obj['message'] = $args[1];
                        $obj['response'] = $args[3];

                        return json_encode($obj);
                    }
                    break;

                default:
                    throw new Exception("[warning] b::$name method not found.\n");
                    break;
            }
        }
    }
?>


