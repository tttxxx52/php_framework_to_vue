<?php
    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ('OPTIONS' == $_SERVER['REQUEST_METHOD']) {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        }

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        }

        exit(0);
    }

    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    require_once 'modules/module_management/module_management_api.php';
    require_once 'include/php/config_json.php';
    define('__ROOT__', dirname(__FILE__));

    $body = (new Main())->run();
    echo $body;

    class Main
    {
        private $module;

        public function __construct()
        {
            $config = config_json::read('config.json.php');
            PDO_mysql::$db_host = $config['db']['db_host'];
            PDO_mysql::$db_name = $config['db']['db_name'];
            PDO_mysql::$db_user = $config['db']['db_user'];
            PDO_mysql::$db_password = $config['db']['db_password'];
            $this->module = $config['default_module'];
            $this->test_mode = $config['test_mode'];
            $this->version = $config['version'];
        }

        public function run()
        {
            $event_message = new event_message($_GET, $_POST);
            $data = json_decode(file_get_contents('php://input'), true);
            $data = json_decode($data['parameters']);
            $data = json_decode(json_encode($data), true);
            $event_message->setPost($data);
            $get = $event_message->getGet();

            if (isset($_GET['module'])) {
                $module = $_GET['module'];
            } else {
                $module = $this->module;
            }
            if ('login' !== $get['module'] || 'do_login_action' !== $get['action']) {
                try {
                    session_start();
                    if (!isset($_SESSION['user'])) {
                        throw new Exception('No user login.', -100);
                    }
                    //權限管理 先註解
                    // $user = $_SESSION['user'];
                    // if(!$this->test_mode){
                    //     $result = module_management_api::check_account_privilege($user, $get['module'], $get['action']);
                    //     if($result['status_code']!=0) throw new Exception ($result['status_message'], $result['status_code']);
                    // }
                } catch (Exception $e) {
                    $return_value['status_code'] = $e->getCode();
                    $return_value['status_message'] = $e->getMessage();

                    return json_encode($return_value);
                }
            }

            require_once 'modules'.'/'.$module.'/'.'action_dispatcher.php';
            $module_object = new action_dispatcher();

            $body = $module_object->doAction($event_message);

            return $body;
        }
    }
