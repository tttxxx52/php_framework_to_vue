<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once __ROOT__.'/modules/login/php_model/login_model.php';
    require_once __ROOT__.'/include/php/util.php';
    class do_login_action implements action_listener
    {
        public function actionPerformed(event_message $em)
        {
            $post = $em->getPost();
            $account = $post['account'];
            $password = $post['password'];
            $login_model = new login_model();
            $row = $login_model->login($account, $password);

            if (0 == count($row)) {
                return  util::returnValue(-1, '帳號密碼錯誤');
            } else {
                session_start();
                $_SESSION['user'] = $row[0]['id'];

                return util::returnValue(0, 'success');
            }
        }
    }
