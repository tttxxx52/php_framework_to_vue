<?php
require_once 'include/php/model.php';
    class login_model extends model
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function login($account, $password)
        {
            $sql ="SELECT * FROM user_profile WHERE user_account = ?  AND password = ? ";
            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute(array($account,$password));
            $error = $stmt->errorInfo();
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $row;
        }

       
    }
