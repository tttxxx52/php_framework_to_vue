<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_insert_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $post = $em->getPost();
            $name = $post['name'];
            $contact_id = $post['contact_id'];
            $sql = "INSERT INTO merchant_profile (name, contact_id) VALUES (?, ?)";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array($name, $contact_id));
            if($result) 
                $msg = '新增成功';
            else
                $msg = "新增失敗" . $name . $contact_id;
            return $msg;
        }        
    }
  