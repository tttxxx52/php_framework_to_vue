<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';

    class show_update_page implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $post = $em->getPost();
            $id = $post['id'];
            $sql = "SELECT * FROM merchant_profile where id=?";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array($id));
            $ds = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return json_encode($ds[0]);
        }    
    }

?>