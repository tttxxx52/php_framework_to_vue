<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_insert_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $post = $em->getPost();
            $name = $post['name'];
            $email = $post['email'];
            $password = $post['password'];
            $tel = $post['tel'];
            $addr = $post['addr'];
            $sql = "INSERT INTO user_profile (name, email, password, tel, addr) VALUES (?, ?, ?, ?, ?)";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array($name, $email, $password, $tel, $addr));
            if($result) 
                $msg = '新增成功';
            else
                $msg = "新增失敗";
            $body=$msg;
            return $body;
        }        
    }
  