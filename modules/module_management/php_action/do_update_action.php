<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_update_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $post = $em->getPost();
            $id = $post['id'];
            $version = $post['version'];
            $conn = PDO_mysql::getConnection();
            $sql = "UPDATE module_profile SET version=:version WHERE id=:id";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array(":version"=>$version, ":id"=>$id));
            if($result){
                $return_value['status_code'] = 0;
                $return_value['status_message'] = '更新成功';
            } else{
                $return_value['status_code'] = -1;
                $return_value['status_message'] = '更新失敗';
                $return_value['sql'] = $sql;
            }
            return json_encode($return_value);
        }    
    }
    
?>
