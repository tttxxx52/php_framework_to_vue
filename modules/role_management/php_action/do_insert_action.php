<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    require_once 'include/php/PDO_mysql.php';
    
    class do_insert_action implements action_listener{
        public function actionPerformed(event_message $em) {
            $conn = PDO_mysql::getConnection();
            $post = $em->getPost();
            $name = $post['name'];
            $comment = $post['comment'];
            $sql = "INSERT INTO role_profile (name, comment) VALUES (?, ?)";
            $stmt = $conn->prepare($sql);
            $result = $stmt->execute(array($name, $comment));
            if($result) 
                $msg = '新增成功';
            else
                $msg = "新增失敗";
            $body=$msg;
            return $body;
        }        
    }
  