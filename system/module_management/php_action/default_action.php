<?php
    require_once 'include/php/action_listener.php';
    require_once 'include/php/event_message.php';
    
    class default_action implements action_listener{
        public function actionPerformed(event_message $event) {
            return "no PHP action is assigned";
        }        
    }
?>